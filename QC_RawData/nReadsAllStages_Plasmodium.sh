# get number of all plasmodium stages

stardirIndo=/data/cephfs/punim0586/kbobowik/STAR/IndoRNA_CombinedPFPX_SecondPass

for file in ${stardirIndo}/IndoOutput/*Log.final.out; do
	shortenedFile=`basename $file Log.final.out`
	sampleID=${shortenedFile##*_}
	a=`echo $sampleID`
	b=`grep 'Number of input reads' $file`
	c=`grep 'Uniquely mapped reads number' $file`
	echo -e "$a\t$b\t$c" >> PlasmodiumSummary_Natri.txt
done


stardirYam=/data/cephfs/punim0586/kbobowik/STAR/Yamagishi_CombinedPFPX_SecondPass
for file in ${stardirYam}/{Controls,Sick}/*Log.final.out; do
	shortenedFile=`basename $file Aligned.sortedByCoord.out.fastqLog.final.out`
	sampleID=${shortenedFile##*_}
	a=`echo $sampleID`
	b=`grep 'Number of input reads' $file`
	c=`grep 'Uniquely mapped reads number' $file`
	echo -e "$a\t$b\t$c" >> PlasmodiumSummary_Yamagishi.txt
done
