# get number of reads at all stages

# First, number of raw reads

rawDir="/data/cephfs/punim0586/shared/raw_data"
summaryDir="/data/cephfs/punim0586/kbobowik/Sumba/SummaryStats"
arrayDir="/data/cephfs/punim0586/kbobowik/Sumba/scripts/Array_Scripts"
trimmedDir="/data/cephfs/punim0586/kbobowik/processed_data"
starDir="/data/cephfs/punim0586/kbobowik/STAR/Hg38_SecondPass"
fcDir="/data/cephfs/punim0586/kbobowik/Sumba/FeatureCounts"

# get raw reads
for file in ${rawDir}/{indoRNA,indoRNA_second_batch,indoRNA_third_batch}/*_1.fastq.gz; do
	shortenedFile=`basename $file _1.fastq.gz`
	batch=`echo $file | cut -d/ -f 7`
	echo "printf \"\`echo -e \$(zcat $file|wc -l)/4|bc\` $batch $shortenedFile\n\"" '>' ${summaryDir}/totalRawReads.txt
done > ${arrayDir}/rawReadsArray.txt


# get trimmed reads
for file in ${trimmedDir}/{indoRNA,indoRNA_second_batch,indoRNA_third_batch}/paired*_1.fastq.gz; do
	shortenedFile=`basename $file _1.fastq.gz`
	sampleID=${shortenedFile##*_}
	batch=`echo $file | cut -d/ -f 7`
	echo "printf \"\`echo -e \$(zcat $file|wc -l)/4|bc\` $batch $sampleID\n\"" '>' ${summaryDir}/${sampleID}_totalTrimmedReads.txt
done > ${arrayDir}/trimmedReadsArray.txt

# get number of reads after STAR
for file in ${starDir}/{sample_output,second_batch/sample_output,third_batch/sample_output}/STAR*Log.final.out; do
	shortenedFile=`basename $file Log.final.out`
	sampleID=${shortenedFile##*_}
	batch=`echo $file | cut -d/ -f 8`
	grepNumber=`grep "Uniquely mapped reads number" $file`
	uniquelyMappedNumber=`echo $grepNumber | cut -f6 -d' '`
	printf "$uniquelyMappedNumber $batch $sampleID \n"
done > ${summaryDir}/STAR_UniquelyMappedReadsNumbers.txt

# featureCounts Numbers
for file in ${fcDir}/{indoRNA,indoRNA_second_batch,indoRNA_third_batch}/sample_counts/Filter*.txt; do
	shortenedFile=`basename $file .txt`
	sampleID=${shortenedFile##*_}
	batch=`echo $file | cut -d/ -f 8`
	nReads=`tail -n +2 $file | cut -f3 | paste -sd+ | bc`
	printf "$nReads $batch $sampleID \n"
done > ${summaryDir}/FeatureCounts_AssignedReadsNumbers.txt


