# script created by KSB, 08.08.18
# Perform DE analysing relationship between islands

### Last edit: KB 03.04.2019

# Load dependencies and set input paths --------------------------

# Load dependencies:
library(edgeR)
library(plyr)
library(NineteenEightyR)
library(RColorBrewer)
library(biomaRt)
library(ggpubr)
library(ggplot2)
library(ggsignif)
library(pheatmap)
library(viridis)
library(gplots)
library(circlize)
library(ComplexHeatmap)
library(EnsDb.Hsapiens.v86)
library(ggsci)
library(scales)
library(dendextend)
library(reshape2)
library(variancePartition)
library(doParallel)
library(preprocessCore)

# Set paths:
inputdir = "/Users/katalinabobowik/Documents/UniMelb_PhD/Analysis/UniMelb_Sumba/Output/Malaria/"
housekeepingdir="/Users/katalinabobowik/Documents/UniMelb_PhD/Analysis/UniMelb_Sumba/BatchEffects/"

# Set output directory and create it if it does not exist:
outputdir <- "/Users/katalinabobowik/Documents/UniMelb_PhD/Analysis/UniMelb_Sumba/Output/Malaria/MergedData/"

if (file.exists(outputdir) == FALSE){
    dir.create(outputdir)
}

# Load colour schemes:
wes=c("#3B9AB2", "#EBCC2A", "#F21A00", "#00A08A", "#ABDDDE", "#000000", "#FD6467","#5B1A18")
palette(c(wes, brewer.pal(8,"Dark2")))
# set up colour palette for batch
study.col=viridis(n=2, alpha=0.8)
village.col=c("#EBCC2A","chocolate","chocolate","#3B9AB2","#F21A00","chocolate","chocolate","chocolate","#78B7C5","orange","chocolate")

# set up colour palette. The 'NPG' palette (Nature Publising Group) from ggsci will be used for the standard palette
standard_col=c(pal_npg()(10), pal_futurama()(12), pal_tron()(3))
plasmo=brewer.pal(3, "Set2")

dev.off()

# load in data files -----------------------------------------------------------------------

# yamagishi data
load(paste0(inputdir, "Yamagishi-Analysis/dataPreprocessing/Yamagishi.read_counts.TMM.filtered_allDiseases.Rda"))
yamagishi=y
# Indo data
load(paste0(inputdir, "Indo-Analysis/dataPreprocessing/indoRNA.read_counts.TMM.filtered.Rda"))
indo=y

# merge samples ----------------------------

# change yamagishi 'instrument' to 'batch' and 'location' to 'island' so that there is consistency in naming between Yamagishi and Indonesian data
colnames(yamagishi$samples)[grep("instrument",colnames(yamagishi$samples))]="batch"
colnames(yamagishi$samples)[grep("location",colnames(yamagishi$samples))]="Island"

# filter for only common genes (rows) and common covariates (columns)
common.genes=rownames(yamagishi)[which(rownames(yamagishi) %in% rownames(indo))]
indo=indo[common.genes,]
yamagishi=yamagishi[common.genes,]
common.covariates=which(colnames(indo$samples) %in% colnames(yamagishi$samples))
indo.index=which(colnames(indo$samples) %in% colnames(yamagishi$samples))
yam.index=which(colnames(yamagishi$samples) %in% colnames(indo$samples))
indo$samples=indo$samples[,indo.index]
yamagishi$samples=yamagishi$samples[,yam.index]
merged = cbind(indo, yamagishi)
dim(merged)
# [1] 11786   245

# drop any unused levels
merged$samples=droplevels(merged$samples)

# covariates -------------

# add covariate for study
indo.study=grep("SMB|MPI|MTW", colnames(merged))
yam.study=grep("DRR", colnames(merged))
merged$samples$study=NA
merged$samples$study[indo.study]="indo"
merged$samples$study[yam.study]="yamagishi"
merged$samples$study=as.factor(merged$samples$study)
merged$samples$diseaseStatus=gsub("control", "healthy", merged$samples$diseaseStatus) %>% gsub("malaria", "sick", .)
merged$samples$diseaseStatus=as.factor(merged$samples$diseaseStatus)
merged$samples$species=gsub("controls", "healthy", merged$samples$species)
merged$samples$species=as.factor(merged$samples$species)

# fill in missing 'NA' values
merged$samples$Island=as.character(merged$samples$Island)
merged$samples$Island[which(is.na(merged$samples$Island))]="Unknown"

# assign covariate names
# subtract variables we don't need
subtract=c("group", "norm.factors")
# get index of unwanted variables
subtract=which(colnames(merged$samples) %in% subtract)
covariate.names = colnames(merged$samples)[-subtract]
for (name in covariate.names){
 assign(name, merged$samples[[paste0(name)]])
}

# library size needs to be broken up into chunks for easier visualisation of trends (for instance in Age, we want to see high age vs low age rather than the effect of every single age variable)
assign("lib.size", cut(as.numeric(as.character(merged$samples$lib.size)), breaks=5))

# assign names to covariate names so you can grab individual elements by name
names(covariate.names)=covariate.names

# assign factor variables
factorVariables=c(colnames(Filter(is.factor,merged$samples))[which(colnames(Filter(is.factor,merged$samples)) %in% covariate.names)], "lib.size")
numericVariables=colnames(Filter(is.numeric,merged$samples))[which(colnames(Filter(is.numeric,merged$samples)) %in% covariate.names)] %>% subset(., !(. %in% factorVariables))

# Perform rarefaction curves for number of expressed genes vs. proportion of pool mRNA (as in Ramskold D, Wang ET, Burge CB, Sandberg R. 2009. An abundance of ubiquitously expressed genes revealed by tissue transcriptome sequence data. PLoS Comput Biol 5:e1000598)
pdf(paste0(outputdir, "rarefactionCurves_mergedData.pdf"))
for (name in factorVariables) {
  plot(1:length(merged$counts[,1]), cumsum(sort(merged$counts[,1], decreasing=T)/sum(merged$counts[,1])), log="x", type="n", xlab="Number of genes", ylab="Fraction of reads pool", ylim=c(0,1), main=name)
  counter=0
  for (sample in colnames(merged)){
    counter=counter+1
    lines(1:length(merged$counts[,sample]), cumsum(sort(merged$counts[,sample], decreasing=T)/sum(merged$counts[,sample])), lwd=2, col=as.numeric(get(name))[counter])
  }
  levels=levels(get(name))
  levels[which(is.na(levels))] = "NA"
  legend(x="bottomright", bty="n", col=1:length(levels(get(name))), legend=levels, lty=1, lwd=2)
}
dev.off()

# Transformation from the raw scale --------------------------------------------------------------------

# Transform raw counts onto a scale that accounts for library size differences. Here, we transform to CPM and log-CPM values (prior count for logCPM = 0.25). 
cpm <- cpm(merged) 
lcpm <- cpm(merged, log=TRUE)

# Removal of lowly-expressed genes -------------------------------------------------------------------------------

# get histogram of number of genes expressed at log2 cpm > 0.5 and 1 (before filtering)
pdf(paste0(outputdir, "lcpm_preFiltering_Histogram.pdf_cpm1.pdf"), height=10, width=15)
par(mfrow=c(1,2))
hist(rowSums(lcpm>0.5), main= "Genes expressed at log2 cpm over 0.5 \n pre-filtering", xlab="samples", col=4, ylim=c(0,16000))
dev.off()

# put samples through filtering paramaters to make sure nothing else needs to be filtered out after combining
cpm_indo <- cpm[,which(merged$samples$study=="indo")]
cpm_yam <- cpm[,which(y$samples$diseaseStatus=="yamagishi")]
keep.indo <- rowSums(cpm_indo>1) >= (ncol(cpm[,which(merged$samples$study=="indo")])*0.5)
keep.yam <- rowSums(cpm_yam>1) >= (ncol(cpm[,which(y$samples$study=="yamagishi")])*0.5)
keep <- keep.indo | keep.yam
y <- merged[keep,, keep.lib.sizes=FALSE]
dim(y)
# [1] 11786   245

# Visualise library size 
pdf(paste0(outputdir, "librarySize_mergedData.pdf"), height=10, width=20)
par(oma=c(5,2,0,0))
barplot(merged$samples$lib.size*1e-6, ylab="Library size (millions)", names=colnames(merged), cex.lab=1.6, cex.names=1.3, cex.axis=1.6, cex.main=1.5, col=study.col[merged$samples$study], las=3, ylim=c(0,max(merged$samples$lib.size*1e-6)+10), main="Library Size")
dev.off()

pdf(paste0(outputdir, "nGenes_mergedData.pdf"), height=10, width=20)
par(oma=c(5,2,0,0))
barplot(apply(merged$counts, 2, function(c)sum(c!=0)),main="Number of Genes", ylab="n Genes", cex.names=0.75, col=study.col[merged$samples$study], names=colnames(merged), las=3, ylim=c(0,max(apply(merged$counts, 2, function(c)sum(c!=0)))+3000))
dev.off()

# get library size density
pdf(paste0(outputdir, "libraryDensity_beforeAndAfterFiltering_mergedData.pdf"), height=8, width=15)
nsamples <- ncol(merged)
par(mar=c(5,7,4,4))
plot(density(lcpm[,1]), ylab='', cex.axis=1.5, cex.lab=1.5, col=study.col[as.numeric(merged$samples$study)][1], lwd=2, ylim=c(0,max(density(lcpm)$y)+.1), las=2, main="", xlab="")
title(main="Library Density", xlab="Log-cpm", cex.main=2, cex.lab=1.5)
title(ylab="Density", line=5, cex.lab=1.5)
abline(v=0, lty=3)
for (i in 2:nsamples){
    den <- density(lcpm[,i])
    lines(den$x, den$y, col=study.col[as.numeric(merged$samples$study)][i], lwd=2)
}
legend("topright", legend=c("Indo","Yamagishi"), ncol=1, cex=1.3, text.col=study.col[unique(merged$samples$study)], bty="n")
dev.off()

# normalise gene-expression distribution -------------------------------------------------

# Test normalising data through TMM normalisation
merged <- calcNormFactors(merged, method="TMM")

# plot how well TMM normalisation worked 
pdf(paste0(outputdir, "NormalisedGeneExpressionDistribution_mergedData_TMM.pdf"), height=15, width=15)
  par(oma=c(2,0,0,0), mfrow=c(2,1), mar=c(5,6,4,4))
  merged2 <- merged
  merged2$samples$norm.factors <- 1
  lcpm <- cpm(merged2, log=TRUE)
  boxplot(lcpm, las=2, col=study.col[as.numeric(study)], main="", cex.axis=1.5, cex.lab=1.5, names=colnames(merged), xlab='', xaxt='n')
  title(main="A. Unnormalised data",ylab="Log-cpm", cex.main=2, cex.lab=1.5)
  merged2 <- calcNormFactors(merged2, method="TMM")
  lcpm <- cpm(merged2, log=TRUE)
  boxplot(lcpm, las=2, col=study.col[as.numeric(study)], main="", xlab='', cex.axis=1.5, cex.lab=1.5, names=colnames(merged), xaxt='n')
  title(main="B. Normalised data, TMM",ylab="Log-cpm", cex.main=2, cex.lab=1.5)
dev.off()

# recalculate lcpm
lcpm=cpm(merged, log=T)

# save data
save(lcpm, file=paste0(outputdir, "merged.logCPM.TMM.filtered.Rda"))
save(merged, file=paste0(outputdir, "merged.read_counts.TMM.filtered.Rda"))

# quantile normalise the data
#normalize.quantiles(merged,copy=TRUE)
#v <- voom(counts, design, plot=TRUE, normalize="quantile")

