# script created by KSB, 08.08.18
# Perform DE analysing relationship between islands

### Last edit: KB 03.04.2019

# Load dependencies and set input paths --------------------------

# Load dependencies:
library(edgeR)
library(plyr)
library(NineteenEightyR)
library(RColorBrewer)
library(biomaRt)
library(ggpubr)
library(ggplot2)
library(ggsignif)
library(pheatmap)
library(viridis)
library(gplots)
library(circlize)
library(ComplexHeatmap)
library(EnsDb.Hsapiens.v86)
library(ggsci)
library(scales)
library(dendextend)
library(reshape2)
library(variancePartition)
library(doParallel)
library(devtools)
library(Biobase)
library(preprocessCore)
library(magrittr)
library(EnhancedVolcano)
library(seqsetvis)
library(UpSetR)

UniqueFalciparumGoResults = read.delim("/Users/katalinabobowik/Documents/UniMelb_PhD/Analysis/UniMelb_Sumba/Output/Malaria/MergedData/ClueGOEnrichmentVivaxFalciparum/ClueGOResults_UniqueFalciparumGenes/ClueGOResults_UniqueFalciparumGenesNodeAttributeTable.txt")
# get mean LFC value of genes in each GOID entry and add that as new column
UniqueFalciparumGoResults$meanLFC = NA
for (n in 1:nrow(UniqueFalciparumGoResults)){
	genesFoundForGO = as.character(UniqueFalciparumGoResults[n,"Associated.Genes.Found"])
	genesFoundForGO.noBrackets = gsub("\\[","", genesFoundForGO) %>% gsub("\\]","", .)
	genesFoundForGO.split = strsplit(genesFoundForGO.noBrackets,",")[[1]]
	genesFoundForGO.nospace = gsub(" ","", genesFoundForGO.split)
	UniqueFalciparumGoResults$meanLFC[n] = mean(topTable.FalcvsHealthy[unique(grep(paste(genesFoundForGO.nospace,collapse="|"), topTable.FalcvsHealthy$SYMBOL)),"logFC"])
}
plot(-log(UniqueFalciparumGoResults$Term.PValue.Corrected.with.Benjamini.Hochberg), UniqueFalciparumGoResults$meanLFC, col=as.numeric(UniqueFalciparumGoResults$GOGroups), pch=19)
text(-log(UniqueFalciparumGoResults$Term.PValue.Corrected.with.Benjamini.Hochberg), UniqueFalciparumGoResults$meanLFC, labels=UniqueFalciparumGoResults$GOGroups)

# Vivax
UniqueVivaxGoResults = read.delim("/Users/katalinabobowik/Documents/UniMelb_PhD/Analysis/UniMelb_Sumba/Output/Malaria/MergedData/PopComparisons/ClueGOResultsForUniqueVivaxGenes/ClueGOResultsForUniqueVivaxGenesNodeAttributeTable.txt")
# get mean LFC value of genes in each GOID entry and add that as new column
UniqueVivaxGoResults$meanLFC = NA
for (n in 1:nrow(UniqueVivaxGoResults)){
	genesFoundForGO = as.character(UniqueVivaxGoResults[n,"Associated.Genes.Found"])
	genesFoundForGO.noBrackets = gsub("\\[","", genesFoundForGO) %>% gsub("\\]","", .)
	genesFoundForGO.split = strsplit(genesFoundForGO.noBrackets,",")[[1]]
	genesFoundForGO.nospace = gsub(" ","", genesFoundForGO.split)
	UniqueVivaxGoResults$meanLFC[n] = mean(topTable.VivaxvsHealthy[unique(grep(paste(genesFoundForGO.nospace,collapse="|"), topTable.VivaxvsHealthy$SYMBOL)),"logFC"])
}
plot(-log(UniqueVivaxGoResults$Term.PValue.Corrected.with.Benjamini.Hochberg), UniqueVivaxGoResults$meanLFC, col=as.numeric(UniqueVivaxGoResults$GOGroups), pch=19)
text(-log(UniqueVivaxGoResults$Term.PValue.Corrected.with.Benjamini.Hochberg), UniqueVivaxGoResults$meanLFC, labels=UniqueVivaxGoResults$GOGroups)


# Heatmap of top goID
topPvalueterm = UniqueFalciparumGoResults[order(UniqueFalciparumGoResults$Term.PValue.Corrected.with.Benjamini.Hochberg),][5,]
genesFoundForGO = as.character(topPvalueterm$Associated.Genes.Found)
genesFoundForGO.noBrackets = gsub("\\[","", genesFoundForGO) %>% gsub("\\]","", .)
genesFoundForGO.split = strsplit(genesFoundForGO.noBrackets,",")[[1]]
genesFoundForGO.nospace = gsub(" ","", genesFoundForGO.split)
topGenesForGO = topTable.FalcvsHealthy[unique(grep(paste(genesFoundForGO.nospace,collapse="|"), topTable.FalcvsHealthy$SYMBOL)),"logFC"]
names(topGenesForGO) = genesFoundForGO.nospace
pdf(paste0(outputdir,"HeatmapTopGenesForGO.pdf"))
Heatmap(topGenesForGO)
dev.off()

# Heatmap of top goID
topPvalueterm = UniqueVivaxGoResults[order(UniqueVivaxGoResults$Term.PValue.Corrected.with.Benjamini.Hochberg),][1,]
genesFoundForGO = as.character(topPvalueterm$Associated.Genes.Found)
genesFoundForGO.noBrackets = gsub("\\[","", genesFoundForGO) %>% gsub("\\]","", .)
genesFoundForGO.split = strsplit(genesFoundForGO.noBrackets,",")[[1]]
genesFoundForGO.nospace = gsub(" ","", genesFoundForGO.split)
topGenesForGO = topTable.VivaxvsHealthy[unique(grep(paste(genesFoundForGO.nospace,collapse="|"), topTable.VivaxvsHealthy$SYMBOL)),"logFC"]
names(topGenesForGO) = genesFoundForGO.nospace
pdf(paste0(outputdir,"HeatmapTopGenesForGO.pdf"))
Heatmap(topGenesForGO)
dev.off()