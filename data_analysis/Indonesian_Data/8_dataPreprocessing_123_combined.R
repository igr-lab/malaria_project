# script created by KSB

# Load dependencies and set input paths --------------------------

# Load dependencies:
library(edgeR)
library(plyr)
library(NineteenEightyR)
library(openxlsx)
library(RColorBrewer)
library(magrittr)
library(ggsci)
library(scales)
library(viridis)

# Set paths:
inputdir = "/Users/katalinabobowik/Documents/UniMelb_PhD/Analysis/UniMelb_Sumba/Output/Malaria/"
covariatedir <- "/Users/katalinabobowik/Documents/UniMelb_PhD/Analysis/UniMelb_Sumba/ReferenceFiles/indoRNA_SequencingFiles/"

# Set output directory and create it if it does not exist:
outputdir <- "/Users/katalinabobowik/Documents/UniMelb_PhD/Analysis/UniMelb_Sumba/Output/Malaria/Indo-Analysis/dataPreprocessing/"

if (file.exists(outputdir) == FALSE){
    dir.create(outputdir)
}

# set up colour palette. The "wes" palette will be used for island and other statistical information, whereas NineteenEightyR will be used for batch information
wes=c("#3B9AB2", "#EBCC2A", "#F21A00", "#00A08A", "#ABDDDE", "#000000", "#FD6467","#5B1A18")
palette(c(wes, brewer.pal(8,"Dark2")))
# set up colour palette for batch
batch.col=electronic_night(n=3)
standard_col=c(pal_npg()(10), pal_futurama()(12), pal_tron()(3))
plasmo=brewer.pal(3, "Set2")
study.col=viridis(n=2, alpha=0.8)
dev.off()

# Load in count data -----------------------------------------------------------

# load in count data (as DGE list object)
load(paste0(inputdir, "Indo-Analysis/dataPreprocessing/unfiltered_DGElistObject.Rda"))

# MPI-296 was found to actually be a female, so we'll take it out of the analysis
y=y[,-which(colnames(y) %in% "MPI-296_firstBatch")]

# Create shortened samplenames- insert hyphen and drop suffixes
samplenames <- as.character(y$samples$samples)
samplenames <- sub("([A-Z]{3})([0-9]{3})", "\\1-\\2", samplenames)
samplenames <- sapply(strsplit(samplenames, "[_.]"), `[`, 1)

# make barplot after removing MPI-296
pdf(paste0(outputdir, "librarySize_indoRNA_removingOutliers_123Combined.pdf"), height=10, width=20)
  par(oma=c(5,2,0,0))
  barplot(y$samples$lib.size*1e-6, ylab="Library size (millions)", cex.lab=1.6, cex.names=1.3, cex.axis=1.6, cex.main=1.5, col=study.col[1], names=samplenames, las=3, ylim=c(0,max(y$samples$lib.size*1e-6)+10), main="Library Size \nIndonesian")
  # abline(h=10, col="red")
  #legend(x="topright", col=batch.col[unique(y$samples$batch)], legend=c("first batch", "second batch", "third batch"), pch=15, cex=1.5)
dev.off()

# Covariate setup --------------------------------------

# load in covariate information, created from script Indo_CovariateData.R
covariates=read.table(paste0(inputdir,"Indo-Analysis/dataPreprocessing/covariates.txt"))
covariates=covariates[-grep("MPI-296_firstBatch",rownames(covariates)),]

# remove any duplicated columns in covariate DF
which(colnames(covariates) %in% colnames(y$samples))
# [1] 1 2 3 4 5
covariates=covariates[,-which(colnames(covariates) %in% colnames(y$samples))]

# check to make sure ordeing of samples is the same
identical(rownames(y$samples), rownames(covariates))
# [1] TRUE

# assign covariates to DGE list
y$samples=cbind(y$samples, covariates)

# add in species and health status information 

# from the script "Indonesian-PlasmodiumData.R", we took all of the plasmodium reads and plotted them. I saw that 6 samples could be ruled out as malaria outliers.
# let's assign these
y$samples$species=NA
falciparum=c("MPI-025_firstBatch", "MPI-345_secondBatch", "SMB-PTB028_thirdBatch", "MPI-334_secondBatch", "MPI-376_firstBatch")
vivax=c("MPI-061_firstBatch")
y$samples[grep(paste(falciparum,collapse="|"), colnames(y)),"species"]="falciparum"
y$samples[grep(paste(vivax,collapse="|"), colnames(y)),"species"]="vivax"
y$samples[which(is.na(y$samples$species)),"species"]="control"
y$samples$species=factor(y$samples$species, levels=c("falciparum","control","vivax"))

# add in health status information
y$samples$diseaseStatus=NA
y$samples$diseaseStatus[which(y$samples$species=="control")]="control"
y$samples$diseaseStatus[which(y$samples$species!="control")]="malaria"
y$samples$diseaseStatus=as.factor(y$samples$diseaseStatus)

# add in library size and batch infromation to covariate names vector
covariate.names = colnames(y$samples)[-which(colnames(y$samples) %in% c("group","norm.factors","samples"))]

# Make sure all covariates in y$samples are the appropriate structure. This is important, as it affects the linear model and ANOVA tests
# Get variables that aren't numeric and transform them into factors. Batch is considered numeric (which we want to change), so we'll also add this into our make.factor vector
which.numeric=unlist(lapply(y$samples, is.numeric))
make.factor=c(colnames(y$samples[,!which.numeric]), "batch")
        
# assign variables and make y-samples metadata into appropriate structure
for (name in make.factor){
  if(sum(is.na(y$samples[[name]])) > 0){
    y$samples[[paste0(name)]]=assign(name, as.factor(addNA(y$samples[[paste0(name)]])))
  }
  else{
    y$samples[[paste0(name)]]=assign(name, as.factor(y$samples[[paste0(name)]]))
  }
}

# now do this for numeric variables
which.numeric=unlist(lapply(y$samples, is.numeric))
make.numeric=colnames(y$samples[,which.numeric])[c(1,3:ncol(y$samples[,which.numeric]))]

# assign numeric information to variables
for (name in make.numeric){
  assign(name, as.numeric(as.character(y$samples[[paste0(name)]])))
}

# Age, RIN, and library size need to be broken up into chunks for easier visualisation of trends (for instance in Age, we want to see high age vs low age rather than the effect of every single age variable)
for (name in c("Age","RIN","lib.size")){
  assign(name, cut(as.numeric(as.character(y$samples[[paste0(name)]])), breaks=5))
}

# Perform rarefaction curves for number of expressed genes vs. proportion of pool mRNA (as in Ramskold D, Wang ET, Burge CB, Sandberg R. 2009. An abundance of ubiquitously expressed genes revealed by tissue transcriptome sequence data. PLoS Comput Biol 5:e1000598)
pdf(paste0(outputdir, "rarefactionCurves_indoRNA_123Combined.pdf"))
for (name in colnames(Filter(is.factor,y$samples))[-c(1,2)]) {
  plot(1:length(y$counts[,1]), cumsum(sort(y$counts[,1], decreasing=T)/sum(y$counts[,1])), log="x", type="n", xlab="Number of genes", ylab="Fraction of reads pool", ylim=c(0,1), main=name) ## initialize the plot area
  counter=0
  for (sample in colnames(y)){
    counter=counter+1
    lines(1:length(y$counts[,sample]), cumsum(sort(y$counts[,sample], decreasing=T)/sum(y$counts[,sample])), lwd=2, col=as.numeric(get(name))[counter])
  }
  levels=levels(get(name))
  levels[which(is.na(levels))] = "NA"
  legend(x="bottomright", bty="n", col=1:length(levels(get(name))), legend=levels, lty=1, lwd=2)
}
# add in batch (we do this separately since it has a different colour)
plot(1:length(y$counts[,1]), cumsum(sort(y$counts[,1], decreasing=T)/sum(y$counts[,1])), log="x", type="n", xlab="Number of genes", ylab="Fraction of reads pool", ylim=c(0,1), main="batch") ## initialize the plot area
counter=0
for (sample in colnames(y)){
  counter=counter+1
  lines(1:length(y$counts[,sample]), cumsum(sort(y$counts[,sample], decreasing=T)/sum(y$counts[,sample])), lwd=2, col=batch.col[as.numeric(batch)][counter])
}
levels=levels(batch)
levels[which(is.na(levels))] = "NA"
legend(x="bottomright", bty="n", col=batch.col[1:length(levels(batch))], legend=levels, lty=1, lwd=2)
dev.off()

# Transformation from the raw scale --------------------------------------------------------------------

# Transform raw counts onto a scale that accounts for library size differences. Here, we transform to CPM and log-CPM values (prior count for logCPM = 0.25). 
cpm <- cpm(y) 
lcpm <- cpm(y, log=TRUE)

# replicate analysis -----------------------------------------------------------------------------------

# set replicate names and rename lcpm columns
allreps=samplenames[which(y$samples$replicate==T)]
allreps=unique(allreps)

# look at how well replicate performed
pdf(paste0(outputdir, "replicate_comparisons_noFiltering_123Combined.pdf"), height=7, width=7)
par(mfrow=c(3,3))
# Since SMB-ANK-027 had two replicates, we need to plot one vs three and two vs three. First, one vs three
smoothScatter(lcpm[,which(samplenames %in% "SMB-ANK-027")[1]], lcpm[,which(samplenames %in% "SMB-ANK-027")[3]], ylab=sapply(strsplit(colnames(lcpm)[which(samplenames %in% allreps[1])[3]], "[_.]"), `[`, 2), xlab=sapply(strsplit(colnames(lcpm)[which(samplenames %in% allreps[1])[1]], "[_.]"), `[`, 2), xlim=c(-5,15), ylim=c(-5,15), main="SMB-ANK-027")
# best fit/regression line
abline(lm(lcpm[,which(samplenames %in% "SMB-ANK-027")[3]]~lcpm[,which(samplenames %in% "SMB-ANK-027")[1]]), col="green")
# diagonal line
abline(a=0,b=1,col="red")

# Now, replicates two vs three
smoothScatter(lcpm[,which(samplenames %in% "SMB-ANK-027")[2]], lcpm[,which(samplenames %in% "SMB-ANK-027")[3]], ylab=sapply(strsplit(colnames(lcpm)[which(samplenames %in% allreps[1])[3]], "[_.]"), `[`, 2), xlab=sapply(strsplit(colnames(lcpm)[which(samplenames %in% allreps[1])[2]], "[_.]"), `[`, 2), xlim=c(-5,15), ylim=c(-5,15), main="SMB-ANK-027")
# best fit/regression line
abline(lm(lcpm[,which(samplenames %in% "SMB-ANK-027")[3]]~lcpm[,which(samplenames %in% "SMB-ANK-027")[2]]), col="green")
# diagonal line
abline(a=0,b=1,col="red")

for (i in 1:length(allreps)){
  smoothScatter(lcpm[,which(samplenames %in% allreps[i])[1]], lcpm[,which(samplenames %in% allreps[i])[2]], ylab=sapply(strsplit(colnames(lcpm)[which(samplenames %in% allreps[i])[2]], "[_.]"), `[`, 2), xlab=sapply(strsplit(colnames(lcpm)[which(samplenames %in% allreps[i])[1]], "[_.]"), `[`, 2), xlim=c(-5,15), ylim=c(-5,15), main=allreps[i])
  # best fit/regression line
  abline(lm(lcpm[,which(samplenames %in% allreps[i])[2]]~lcpm[,which(samplenames %in% allreps[i])[1]]), col="green")
  # diagonal line
  abline(a=0,b=1,col="red")
}
dev.off()

# Removal of lowly-expressed genes -------------------------------------------------------------------------------

# get histogram of number of genes expressed at log2 cpm > 0.5 and 1 (before filtering)
pdf(paste0(outputdir, "lcpm_preFiltering_Histogram.pdf_cpm1.pdf"), height=10, width=15)
par(mfrow=c(1,2))
hist(rowSums(lcpm>0.5), main= "Genes expressed at log2 cpm over 0.5 \n pre-filtering", xlab="samples", col=4, ylim=c(0,16000))
hist(rowSums(lcpm>1), main= "Genes expressed at log2 cpm over 1 \n pre-filtering", xlab="samples", col=5, ylim=c(0,16000))
dev.off()

# filtering option 1 for sick samples 
cpm_sick <- cpm[,which(y$samples$diseaseStatus=="malaria")]
cpm_healthy <- cpm[,which(y$samples$diseaseStatus=="control")]
keep.sick <- rowSums(cpm_sick>1) >= (ncol(cpm[,which(y$samples$diseaseStatus=="malaria")])*0.5)
keep.healthy <- rowSums(cpm_healthy>1) >= (ncol(cpm[,which(y$samples$diseaseStatus=="control")])*0.5)
keep <- keep.sick | keep.healthy
y <- y[keep,, keep.lib.sizes=FALSE]
dim(y)
# [1] 12743   122

# Visualise library size after filtering with barplots
pdf(paste0(outputdir, "librarySize_indoRNA_postFiltering_123Combined.pdf"), height=10, width=20)
  par(oma=c(5,2,0,0))
  barplot(y$samples$lib.size*1e-6, ylab="Library size (millions)", cex.lab=1.6, cex.names=1.3, cex.axis=1.6, cex.main=1.5, col=study.col[1], names=samplenames, las=3, ylim=c(0,max(y$samples$lib.size*1e-6)+10), main="Library Size \nIndonesian")
  # abline(h=10, col="red")
  #legend(x="topright", col=batch.col[unique(y$samples$batch)], legend=c("first batch", "second batch", "third batch"), pch=15, cex=1.5)
dev.off()

pdf(paste0(outputdir, "nGenes_indoRNA_postFiltering_123Combined.pdf"), height=10, width=15)
  par(oma=c(2,0,0,0))
  barplot(apply(y$counts, 2, function(c)sum(c!=0)),main="Number of Genes \n Post-Filtering", ylab="n Genes", cex.names=0.75, col=batch.col[y$samples$batch], names=samplenames, las=3, ylim=c(0,max(apply(y$counts, 2, function(c)sum(c!=0)))+3000))
  legend(x="topright", col=batch.col[unique(y$samples$batch)], legend=c("first batch", "second batch", "third batch"), pch=15, cex=0.8)
dev.off()

# Compare library size density before and after removing lowly-expressed genes
pdf(paste0(outputdir, "libraryDensity_afterFiltering_indoRNA.pdf"), height=8, width=15)
  nsamples <- ncol(y)
  par(mfrow=c(1,2), mar=c(5,7,4,4))
  plot(density(lcpm[,1]), ylab='', cex.axis=1.5, cex.lab=1.5, col=plasmo[y$samples$diseaseStatus][1], lwd=2, ylim=c(0,max(density(lcpm)$y)+.1), las=2, main="", xlab="")
  title(main="All genes", xlab="Log-cpm", cex.main=2, cex.lab=1.5)
  title(ylab="Density", line=5, cex.lab=1.5)
  abline(v=0, lty=3)
  for (i in 2:nsamples){
      den <- density(lcpm[,i])
      lines(den$x, den$y, col=plasmo[y$samples$diseaseStatus][i], lwd=2)
  }
  legend("topright", legend=as.character(unique(y$samples$diseaseStatus)), ncol=1, cex=1.3, text.col=plasmo[unique(y$samples$diseaseStatus)], bty="n")

  lcpm <- cpm(y, log=TRUE)
  plot(density(lcpm[,1]), ylab='', cex.axis=1.5, cex.lab=1.5, col=plasmo[y$samples$diseaseStatus][1], lwd=2, ylim=c(0,max(density(lcpm)$y)+.2), las=2, main="", xlab="")
  title(main="Filtered genes", xlab="Log-cpm", cex.main=2, cex.lab=1.5)
  abline(v=0, lty=3)
  for (i in 2:nsamples){
      den <- density(lcpm[,i])
      lines(den$x, den$y, col=plasmo[y$samples$diseaseStatus][i], lwd=2)
  }
  legend("topright", legend=as.character(unique(y$samples$diseaseStatus)), ncol=1, cex=1.3, text.col=plasmo[unique(y$samples$diseaseStatus)], bty="n")
dev.off()

# get histogram of lcpm
pdf(paste0(outputdir, "lcpm_postFiltering_Histogram.pdf"), height=10, width=15)
  par(mfrow=c(1,2))
  hist(rowSums(lcpm>0.5), main= "Genes expressed at log2 cpm over 0.5 \n post-filtering", xlab="samples", col=4)
  hist(rowSums(lcpm>1), main= "Genes expressed at log2 cpm over 1 \n post-filtering", xlab="samples", col=5)
dev.off()

# replicate analysis after removal of lowly-expressed genes ----------------------------------------------------------------

# get correlation of technical replicates after filtering for lowly expressed genes
pdf(paste0(outputdir, "replicate_comparisons_postFiltering_123Combined.pdf"), height=7, width=7)
# look at how well replicate performed
par(mfrow=c(3,3))
# Since SMB-ANK-027 had two replicates, we need to plot one vs three and two vs three. First, one vs three
smoothScatter(lcpm[,which(samplenames %in% "SMB-ANK-027")[1]], lcpm[,which(samplenames %in% "SMB-ANK-027")[3]], ylab=sapply(strsplit(colnames(lcpm)[which(samplenames %in% allreps[1])[3]], "[_.]"), `[`, 2), xlab=sapply(strsplit(colnames(lcpm)[which(samplenames %in% allreps[1])[1]], "[_.]"), `[`, 2), xlim=c(-5,15), ylim=c(-5,15), main="SMB-ANK-027", cex.lab=1.5)
# best fit/regression line
abline(lm(lcpm[,which(samplenames %in% "SMB-ANK-027")[3]]~lcpm[,which(samplenames %in% "SMB-ANK-027")[1]]), col="green")
# diagonal line
abline(a=0,b=1,col="red")

# Now, replicates two vs three
smoothScatter(lcpm[,which(samplenames %in% "SMB-ANK-027")[2]], lcpm[,which(samplenames %in% "SMB-ANK-027")[3]], ylab=sapply(strsplit(colnames(lcpm)[which(samplenames %in% allreps[1])[3]], "[_.]"), `[`, 2), xlab=sapply(strsplit(colnames(lcpm)[which(samplenames %in% allreps[1])[2]], "[_.]"), `[`, 2), xlim=c(-5,15), ylim=c(-5,15), main="SMB-ANK-027",cex.lab=1.5)
# best fit/regression line
abline(lm(lcpm[,which(samplenames %in% "SMB-ANK-027")[3]]~lcpm[,which(samplenames %in% "SMB-ANK-027")[2]]), col="green")
# diagonal line
abline(a=0,b=1,col="red")

for (i in 1:length(allreps)){
  smoothScatter(lcpm[,which(samplenames %in% allreps[i])[1]], lcpm[,which(samplenames %in% allreps[i])[2]], ylab=sapply(strsplit(colnames(lcpm)[which(samplenames %in% allreps[i])[2]], "[_.]"), `[`, 2), xlab=sapply(strsplit(colnames(lcpm)[which(samplenames %in% allreps[i])[1]], "[_.]"), `[`, 2), xlim=c(-5,15), ylim=c(-5,15), main=allreps[i], cex.lab=1.5)
  # best fit/regression line
  abline(lm(lcpm[,which(samplenames %in% allreps[i])[2]]~lcpm[,which(samplenames %in% allreps[i])[1]]), col="green")
  # diagonal line
  abline(a=0,b=1,col="red")
}
dev.off()

# normalise gene-expression distribution -------------------------------------------------

# eliminate composition bias between libraries by upper quartile normalisation (this will be used in the 'data exploration' step)
y <- calcNormFactors(y, method="TMM")

# plot how well TMM normalisation worked
pdf(paste0(outputdir, "NormalisedGeneExpressionDistribution_IndoRNA_TMM.pdf"), height=15, width=15)
  par(oma=c(2,0,0,0), mfrow=c(2,1), mar=c(5,6,4,4))
  y2 <- y
  y2$samples$norm.factors <- 1
  lcpm <- cpm(y2, log=TRUE)
  boxplot(lcpm, las=2, col=batch.col[as.numeric(batch)], main="", cex.axis=1.5, cex.lab=1.5, names=samplenames, xlab='', xaxt='n')
  title(main="Unnormalised data",ylab="Log-cpm", cex.main=2, cex.lab=1.5)
  y2 <- calcNormFactors(y2, method="TMM")
  lcpm <- cpm(y2, log=TRUE)
  boxplot(lcpm, las=2, col=batch.col[as.numeric(batch)], main="", xlab='', cex.axis=1.5, cex.lab=1.5, names=samplenames, xaxt='n')
  title(main="Normalised data",ylab="Log-cpm", cex.main=2, cex.lab=1.5)
dev.off()

# recalculate lcpm
lcpm=cpm(y, log=T)

# save data
save(lcpm, file=paste0(outputdir, "indoRNA.logCPM.TMM.filtered.Rda"))
save(y, file=paste0(outputdir, "indoRNA.read_counts.TMM.filtered.Rda"))
# covariate matrix
save(covariates, file=paste0(outputdir, "covariates.Rda"))
