# by KSB, 09.22.19

# load packages
library(Rsubread)
library(RColorBrewer)
library(edgeR)
library(Homo.sapiens)
library(limma)
library(ensembldb)
library(EnsDb.Hsapiens.v75)
library(AnnotationDbi)
library(readr)
library(openxlsx)
library(pheatmap)
library(devtools)
library(ggbiplot)
library(biomaRt)
library(biomartr)
library(gplots)
library(sva)
library(topGO)
library(magrittr)
library(dendextend)
library(qvalue)
library(base)
library(gridExtra)

# Set paths:
inputdir = "/Users/katalinabobowik/Documents/UniMelb_PhD/Analysis/UniMelb_Sumba/Output/"
refdir <- "/Users/katalinabobowik/Documents/UniMelb_PhD/Analysis/UniMelb_Sumba/ReferenceFiles/"
outputdir="/Users/katalinabobowik/Documents/UniMelb_PhD/Analysis/UniMelb_Sumba/Output/Malaria/GenderClassification/"

# now set up coulor palette for plasmodium
plasmo=brewer.pal(3, "Set2")
sex=c("red","blue", "black")

# load in count data (as DGE list object)
load(paste0(inputdir, "Malaria/Yamagishi-Analysis/dataPreprocessing/unfiltered_DGElistObject.Rda"))
dim(y)
# [1] 27413   147

# Load in Yamagishi et al 2014 supplementary table 11 (with patient information)
sup11.sick=read.xlsx(paste0(refdir,"Yamagishi/Supplemental_Table_11.xlsx"), sheet=1)
sup11.controls=read.xlsx(paste0(refdir,"Yamagishi/Supplemental_Table_11.xlsx"), sheet=3)
sup11.all=rbind(sup11.sick, sup11.controls)

# load in SRA run table info for sick samples and controls
sra.sick=read.delim("/Users/katalinabobowik/Documents/UniMelb_PhD/Analysis/UniMelb_Sumba/ReferenceFiles/SraRunTable.txt", as.is=T, strip.white=T)
sra.controls=read.delim("/Users/katalinabobowik/Documents/UniMelb_PhD/Analysis/UniMelb_Sumba/ReferenceFiles/Yamagishi/SraRunTable_Controls.txt", as.is=T, strip.white=T)
sra.sick=sra.sick[,c("Library_Name_s","Run_s")]
sra.controls=sra.controls[,c("Library_Name","Run")]
colnames(sra.sick)=colnames(sra.controls)
sra.all=rbind(sra.sick,sra.controls)

# Create empty matrix and fill in with patient names, Age, Sex, and SRA Run ID
sample_summary=sup11.all[,-which(names(sup11.all) %in% "Patient_ID")]
# convert to data frame
sample_summary=as.data.frame(sample_summary)
# get rid of unnecessary characters whic may cause problems later
colnames(sample_summary)=gsub("%","",colnames(sample_summary))
# There is a discrepancy between the SRA table and Supplementary 11 table- one says "malaria7#09" and one says "malaria7#009". We need to change "malaria7#09" to "malaria7#009"
sample_summary$Patient_Name=gsub("malaria7#09", "malaria7#009", sample_summary$Patient_Name)
# Match patient names with the patient names in the SRA table, then grab the corresponding SRA run IDs
sra.all[match(sample_summary$Patient_Name, sra.all$Library_Name),"Run"]
# we seem to have NA values in the dataframe. Which ones are these?
sample_summary$Patient_Name[which(is.na(match(sample_summary$Patient_Name, sra.all$Library_Name)))]
# [1] "malaria11#1" "malaria11#2" "malaria11#3" "malaria11#4" "malaria11#5"
# [6] "malaria11#6" "malaria11#7" "malaria11#8" "malaria11#9"

# when checking the sra run table sheet, these samples have a 0 in front of them. Let's take this out to keep sample names the same.
sra.all$Library_Name=gsub("malaria11#0","malaria11#", sra.all$Library_Name)
sample_summary[,"SRA_ID"]=sra.all[match(sample_summary$Patient_Name, sra.all$Library_Name),"Run"]

# Assign gender, age, location, and PF load
y$samples$gender <- as.factor(sample_summary[match(colnames(y), sample_summary$SRA_ID),"Sex"])
length(which(is.na(y$samples$gender)))
# 13
y$samples$gender <- addNA(as.factor(gsub(" ", "", y$samples$gender)))

# Transformation from the raw scale --------------------------------------------------------------------

# Transform raw counts onto a scale that accounts for library size differences. Here, we transform to CPM and log-CPM values (prior count for logCPM = 0.25). 
cpm <- cpm(y) 
lcpm <- cpm(y, log=TRUE)

# XIST has been suggested to be a good marker for female-specific expression and sex idnetification, as well as a few other male-specific genes outlined in the paper "Robust and tissue-independent gender-specific transcript biomarkers". Let's see how this looks like in the Yamagshi dataset-
sexSpecificGenes=c("XIST", "RPS4Y1", "EIF1AY", "DDX3Y", "KDM5D")

pdf(paste0(outputdir, "SexMarker_GeneExpression.pdf"), width=15)
par(mar=c(8,5,5,1))
for(gene in sexSpecificGenes){
  barplot(cpm[y$genes$ENSEMBL[grep(gene, y$genes$SYMBOL)], ], las=3, col=sex[as.numeric(as.factor(y$samples$gender))], main=paste0(gene," Expression"), ylab="CPM")
  legend(x="topright", col=unique(sex[as.numeric(as.factor(y$samples$gender))]), legend=sapply(1:3, function(x) toString(unique(y$samples$gender)[x])), pch=15, cex=0.8)
}
dev.off()

# plot XIST gene expression using ggplo2 (to make it look nice) and an example y chromosome gene, RPS4Y1
pdf(paste0(outputdir, "XIST_RPS4Y1_Yamagishi_GeneExpression.pdf"), width=15)
sexAssignment <- as.character(y$samples$gender)
samples <- colnames(y)
CPM <- unname(cpm[y$genes$ENSEMBL[grep("XIST", y$genes$SYMBOL)], ])
data <- data.frame(sexAssignment,samples,CPM)
# p1=ggplot(data, aes(fill=sexAssignment, y=CPM, x=samples)) + 
# geom_bar(position="dodge", stat="identity") + theme(axis.text.x = element_text(angle = 90, hjust = 1), panel.background = element_blank(), axis.line = element_line(colour = "grey")) + ggtitle("XIST")
p1=ggplot(data, aes(fill=sexAssignment, y=CPM, x=samples)) + 
geom_bar(position="dodge", stat="identity") + theme(axis.text.x = element_blank(), axis.title.x=element_text(size=14), axis.title.y=element_text(size=14), axis.text=element_text(size=12), axis.ticks.x=element_blank(), panel.background = element_blank(), legend.text=element_text(size=15),legend.title=element_text(size=15),plot.title = element_text(size = 20)) + ggtitle("XIST")
CPM <- unname(cpm[y$genes$ENSEMBL[grep("RPS4Y1", y$genes$SYMBOL)], ])
data <- data.frame(sexAssignment,samples,CPM)
p2 = ggplot(data, aes(fill=sexAssignment, y=CPM, x=samples)) + 
geom_bar(position="dodge", stat="identity") + theme(axis.text.x = element_blank(), axis.title.x=element_text(size=14), axis.title.y=element_text(size=14), axis.text=element_text(size=12), panel.background = element_blank(), axis.ticks.x=element_blank(), legend.text=element_text(size=15),legend.title=element_text(size=15),plot.title = element_text(size = 20)) + ggtitle("RPS4Y1")
grid.arrange(p1, p2, ncol=2)
dev.off()

# let's get the proportion of genes on the x and y chromosomes
xchrom=colSums(cpm[y$genes$ENSEMBL[grep("chrX", y$genes$TXCHROM)], ])
ychrom=colSums(cpm[y$genes$ENSEMBL[grep("chrY", y$genes$TXCHROM)], ])
propy=ychrom/xchrom
propx=xchrom/ychrom
# scale up y chromosome cpm so you can see it on the barplot
propy=propy*100000
propSexChrom=data.frame(propy=propy,propx=propx)

# Make a stacked barplot
pdf(paste0(outputdir,"ProportionXandYGenes.pdf"), width=15)
par(oma=c(8,5,5,1))
barplot(t(propSexChrom), col=c("blue","red") , border="white", las=3, main="Proportion of X and Y Genes")
legend(x="topright", col=c("blue","red"), legend=c("Y-chrom","X-chrom"), pch=15, cex=0.8)
dev.off()

# now just plot proprtion of X-chrom genes
pdf(paste0(outputdir,"ProprtionXGenes.pdf"), width=15)
par(oma=c(8,5,5,1))
barplot(propx, col=sex[as.numeric(y$samples$gender)] , border="white", las=3, main="Prop X Genes/Prop Y Genes", ylab="Prop X Genes")
legend(x="topright", col=c("red", "blue"), legend=c("female","male"), pch=15, cex=0.8)
dev.off()

# now just plot proprtion of X-chrom genes
pdf(paste0(outputdir,"ProprtionYGenes.pdf"), width=15)
par(oma=c(8,5,5,1))
barplot(propy, col=sex[as.numeric(y$samples$gender)] , border="white", las=3, main="Prop Y Genes/Prop X Genes", ylab="Prop Y Genes")
legend(x="topright", col=c("red", "blue"), legend=c("female","male"), pch=15, cex=0.8)
dev.off()

# Now plot cpm of X and Y genes
ySamples=y[grep("chrY", y$genes$TXCHROM),]
xSamples=y[grep("chrX", y$genes$TXCHROM),]
pdf(paste0(outputdir,"MDSXandY.pdf"), width=10)
plotMDS(cpm(ySamples, log=T), col=sex[as.numeric(as.factor(ySamples$samples$gender))], main="Y Chrom Expression")
plotMDS(cpm(xSamples, log=T), col=sex[as.numeric(as.factor(ySamples$samples$gender))], main="X Chrom Expression")
dev.off()

# Extract CPM information by group
cpm_control <- cpm[,grep("control",y$samples$diseaseStatus)]
cpm_sick <- cpm[,grep("malaria",y$samples$diseaseStatus)]

# set keep threshold to only retain genes that are expressed at or over a CPM of one in at least half of the group
keep.control <- rowSums(cpm_control>1) >= (ncol(cpm_control)*0.5)
keep.sick <- rowSums(cpm_sick>1) >= (ncol(cpm_sick)*0.5)

keep <- keep.control | keep.sick
y <- y[keep,, keep.lib.sizes=FALSE]
dim(y)
# [1] 12409 147

# Normalise gene expression distributions (i.e., no bias introduced during sample preparation/sequencing)
y <- calcNormFactors(y, method = "TMM")

# plotMDS again after filtering and noralising data
# Now plot cpm of X and Y genes
ySamples=y[grep("chrY", y$genes$TXCHROM),]
xSamples=y[grep("chrX", y$genes$TXCHROM),]
pdf(paste0(outputdir,"MDSXandY_filtered_normalised.pdf"), width=10)
sex.MDS.y=plotMDS(cpm(ySamples, log=T), col=sex[as.numeric(as.factor(ySamples$samples$gender))], main="Y Chrom Expression")
sex.MDS.x=plotMDS(cpm(xSamples, log=T), col=sex[as.numeric(as.factor(ySamples$samples$gender))], main="X Chrom Expression")
dev.off()

# reassign male and female samples based on MDS plot grouping of teh y chromosome
y$samples$gender[which(sex.MDS.y$x<0)]="F"
y$samples$gender[which(sex.MDS.y$x>0)]="M"
y$samples$gender=droplevels(y$samples$gender)

# replot to make sure everything is correct
pdf(paste0(outputdir,"MDSXandY_sexReassignmnet.pdf"), width=15)
plotMDS(cpm(ySamples, log=T), col=sex[as.numeric(as.factor(y$samples$gender))], main="Y Chrom Expression")
dev.off()

# looks good! Save this and use this for future covariate information
write.table(y$samples$gender, file=paste0(outputdir,"updated_sexCovariate_Yamagishi.txt"), col.names="sex", row.names=colnames(y), quote=F)